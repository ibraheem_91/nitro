#!/bin/bash
# execute the code sniffer
#
# deactivate it with
#  $ git config pre-commit.phpcs 0
# set preferred standard with
#  $ git config pre-commit.phpcs-standard PEAR

if [ "$#" -eq 0 ]; then
    exit
fi
hookfile=`perl -e 'use Cwd "abs_path";print abs_path(shift)' $0`
HOOKHOME="`dirname "$hookfile"`"


PHPCS=./vendor/bin/phpcs
PHPCBF=./vendor/bin/phpcbf
STANDARD="$HOOKHOME/../../tools/phpcs/tajawal_ruleset.xml"
IGNORE=""
ENCODING="--encoding=utf-8"
IGNORE_WARNINGS="-n"

if [ "$(git config --get pre-commit.phpcs)" = "0" ]; then
    # deactivated
    exit 0
fi

TMP="$(git config --get pre-commit.phpcs-standard)"
if [ "$TMP" != "" ]; then
    STANDARD="$TMP"
fi

TMP="$(git config --get pre-commit.phpcs-encoding)"
if [ "$TMP" != "" ]; then
    ENCODING="--encoding=$TMP"
fi

if [ "$PHPCS_IGNORE" != "" ]; then
    IGNORE="--ignore=$PHPCS_IGNORE"
fi

if [ "$PHPCS_IGNORE_WARNINGS" = "1" ]; then
    IGNORE_WARNINGS="-n"
fi

CBFOUTPUT=$($PHPCBF -s $IGNORE_WARNINGS -vv --standard=$STANDARD $ENCODING $IGNORE "$@")
if [[ $CBFOUTPUT != *"No fixable errors"* ]]
then
  echo -e $CBFOUTPUT
  echo -e "\n============================\n";
  echo -e "PHP Code Fixer has fixed some code. please add them and commit again.";
  echo -e "============================\n";
  exit -1
fi

OUTPUT=$($PHPCS -s $IGNORE_WARNINGS --standard=$STANDARD $ENCODING $IGNORE "$@")
RETVAL=$?

if [ $RETVAL -ne 0 ]; then
    echo "$OUTPUT" 1>&2
fi

exit $RETVAL