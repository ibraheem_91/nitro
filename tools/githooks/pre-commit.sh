#!/bin/bash

TMP_STAGING=""

hookfile=`perl -e 'use Cwd "abs_path";print abs_path(shift)' $0`
HOOKHOME="`dirname "$hookfile"`"

# stolen from template file
if git rev-parse --quiet --verify HEAD > /dev/null; then
    against=HEAD
else
    # Initial commit: diff against an empty tree object
    against=b086b000883d7ea2c043a95c099d7c7308051b31
fi

# this is the magic:
# retrieve all files in staging area that are added, modified or renamed
# but no deletions etc
FILES=$(git diff --cached --no-renames --diff-filter=AM --name-only -- | grep -v '^tools/' | grep -v '^app/gii/' | grep -v '^app/migrations/' | grep -v '^app/provider/Amadeus/App/' | grep -v '^app/provider/Hiker/' )
#FILES=$(git diff-index --name-only --cached --diff-filter=ACMR $against -- )


if [ -z "$FILES" ]; then
    exit 0
fi

# match files against whitelist
FILES_TO_CHECK=""
for FILE in $FILES; do
    EXT=${FILE##*.}

    CHECK=1
    case $EXT in
        "php" | "phtml" )
            PHP_FILES="$PHP_FILES $FILE"
            #LINE_FILES="$LINE_FILES $FILE"
            ;;
        htm | html | xml )
            XML_FILES="$XML_FILES $FILE"
            #LINE_FILES="$LINE_FILES $FILE"
            ;;
        js )
            JS_FILES="$JSON_FILES $FILE"
            LINE_FILES="$LINE_FILES $FILE"
            ;;
        json )
            JSON_FILES="$JSON_FILES $FILE"
            ;;
        css | rst | tpl | txt | ts | tss | tsc )
            LINE_FILES="$LINE_FILES $FILE"
            ;;
        * )
            CHECK=0
            ;;
    esac

    if [ "$CHECK" -eq "1" ]; then
        FILES_TO_CHECK="$FILES_TO_CHECK $FILE"
    fi
done

if [ -z "$FILES_TO_CHECK" ]; then
    exit 0
fi

# create temporary copy of staging area
#if [ -e $TMP_STAGING ]; then
#    rm -rf $TMP_STAGING
#fi
#mkdir $TMP_STAGING


# Copy contents of staged version of files to temporary staging area
# because we only want the staged version that will be commited and not
# the version in the working directory
STAGED_FILES=""

HAS_ERROR=0
#for FILE in $FILES_TO_CHECK; do
#    ID=$(git diff-index --cached $against $FILE | cut -d " " -f4)
#
#    # create staged version of file in temporary staging area with the same
#    # path as the original file so that the phpcs ignore filters can be
#    # applied
#    mkdir -p "$(dirname $FILE)"
#    STAGED_FILE="$FILE"
#    git cat-file blob $ID > "$STAGED_FILE"
#    STAGED_FILES="$STAGED_FILES $STAGED_FILE"
#done

for FILE in $PHP_FILES; do
    "$HOOKHOME/pre-commit-check-php" "$FILE"; RET=$?
    if [ $RET -ne 0 ]; then
        HAS_ERROR=1
        continue
    fi
done
for FILE in $XML_FILES; do
    "$HOOKHOME/pre-commit-check-xml" "$FILE"; RET=$?
    if [ $RET -ne 0 ]; then
        HAS_ERROR=1
        continue
    fi
done

for FILE in $JSON_FILES; do
    "$HOOKHOME/pre-commit-check-json" "$FILE"; RET=$?
    if [ $RET -ne 0 ]; then
        HAS_ERROR=1
        continue
    fi
done

#for FILE in $JS_FILES; do
#    "$HOOKHOME/pre-commit-check-js" "$FILE"; RET=$?
#    if [ $RET -ne 0 ]; then
#        HAS_ERROR=1
#        continue
#    fi
#done

for FILE in $LINE_FILES; do
    "$HOOKHOME/pre-commit-check-linestyle" "$FILE"; RET=$?
    if [ $RET -ne 0 ]; then
        HAS_ERROR=1
        continue
    fi
done

if [ $HAS_ERROR -eq 0 ]; then
    "$HOOKHOME/pre-commit-check-phpcs" $PHP_FILES; RET=$?
    if [ $RET -ne 0 ]; then
        HAS_ERROR=1
    fi

#    "$HOOKHOME/pre-commit-check-jshint" $JS_FILES; RET=$?
#    if [ $RET -ne 0 ]; then
#        HAS_ERROR=1
#    fi
fi

# delete temporary copy of staging area
# rm -rf $TMP_STAGING

if [ $HAS_ERROR -ne 0 ]; then
    exit 1
fi
