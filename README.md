# Nitro - REST APIs Made Easy

Creating RESTful APIs with Lumen made easy

Nitro is a package for Lumen 5.* to assist in creation of RESTful APIs. Out of the box it comes with the following:

## Features

- **Model based transformer** All you have to do is specify a `transform` function inside your model and any data associated to that model will be automatically transformed
- **Custom Transformer** you can transform any array, collection or object as well
- **RESTful Exceptions** For *any* exception, custom or system based it will be converted to json following the [API Problem](Specification)
- Also for the exceptions, a trait has been added so you can easily use that in any of your class and easily throw Exceptions will be turned to JSON
- **Commonly used query scopes** some commonly used generic query scopes have been added in the base models which you can easily use
- **Paginated response** not only nitro has support to handle the paginated response but it can also generate paginated response from the collection
- **Avoid the response headache** you can directly return your models or use the helpers provided in the `BaseController` to return the response and it will be automatically converted to JSON with suitable status code also there are certain other helpers available to manipulate the response
- Support for both **Eloquent and [Moloquent](https://github.com/jenssegers/laravel-mongodb)**
- Easy **Model based validation**
-  **[Whoops] (https://filp.github.io/whoops/)** Integrated


## Step by Step Guide

#### 1. Installation - Hatching them!
Since the package is not available on packagist, so add this code to your composer.json to install it:
    
    "repositories": [
        {
             "url" : "https://github.com/tajawal/nitro.git",
             "type": "git"
           }
    ],
    "require": {
        "tajawal/nitro": "dev-master"
    }
    
####2. Registring Lumen
After the package has been installed, you need to follow the following steps in order to let Lumen know about the package and get it to work

- Remove the existing Exception handler i.e. `App\Exceptions\Handler::class` from `$app->singleton` at line 42 and replace it with 

  ```
  Nitro\Exceptions\Handler::class
  ```

- Now register the following global middleware inside `$app->middleware`

  ```
  $app->middleware([
    ...
    ...
    Nitro\Middlewares\TransformerMiddleware::class,
  ]);
  ```

- Now register the service provider by adding the following:

  ```
  $app->register(\Nitro\NitroServiceProvider::class);
  ```
  
### 3. Setting things Up

#### a. Controllers
Extend all your controllers from `Nitro\Controllers\BaseController`

First things first, you should make sure to extend all your controllers from `Nitro\Controllers\BaseController` in order to reap the benefits of using response helpers and easy exceptions

#### b. Models
- Extend your Eloquent Models from `Nitro\Models\BaseEloquent`
- Extend all the Moloquent Models from `Nitro\Models\BaseMoloquent`

### 4. Detailed Usage Guide

Below is the detailed guide on how to use the package for your APIs

#### a. Automatic Transformers

If you would like lumen to automatically transform your models, all you need to do is create a `transform` method inside the model. Let's say that we have a `user` table/collection with associated model `User`. The table looks like below

|Column|Type|Sample Data|
|---|---|---|
|_id|`ObjectId`|234a92waka123aA2k|
|name|`string`|coo coo|
|skills|`array`|`["flying", "picking", "cooing"]`|
|created_at|`string`|2015-01-01 # Side Note: Store them as MongoDate ;)|

Let's say that you want to transform the data to avoid `_id`, add an additional field for `skill_count` and turn the date `created_at` as epoch time and return it as `created_date` name. Then you can add a simple `transform` method inside your model i.e.

```php

...
class User extends BaseEloquent {
    ...
    ...
    /**
     * Nitro will automatically use this method to transform your data
     *
     * @param $user    Consider it as an instance of this model
     *
     * @return array
     */
    public function transform($user) {
        return [
            'name' => $user['name'],
            'skills' => $user['skills'],
            'skill_count' => count($user['skills']),
            'created_date' => strtotime($user['created_at'])
        ];
    }
}
```

#### b. Manually Transform Data

Apart from the automatic transformation, you can also manually transform data in model. Let's take the sampe example above i.e. we have the `User` model with `transform` function and `users` collection with the columns specified above. Now lets say that we want to transform the data during some processing you can simply do it by passing the model data to `transformModel` method i.e.

```php
$content = $this->user->getAll();

$transformer = new Transformer();
$transformedContent = $transformer->transformModel($content);  // Make sure that you have `transform` method inside your model
```


#### c. Custom Transformer

If you do not always want to transform your model data of if you have some data from an external source or if you have a custom data that you magically generated, you can use custom transformer to transform that data i.e.

```php

// The data that we would like to transform
$content = $this->voodooSpells();

$transformer = new Transformer();
$transformer->forceTransform($content, function ($item) {
    return [
        'name'        => $item['name'],
        'position'    => $item['responsibility'],
        'skills'      => $item['skills'],
        'skill_count' => count($item['skills']),
    ];
});
```

#### d. Exceptions

According to the REST advocates, HTTP status codes must be used in the response, but unforutnately a little has been done in standardizing the response format. 

However for the JSON APIs, there are two formats which are starting to achieve large adoption `application/vnd.error+json and application/problem+json`. Nitro provides the support for the latter, which goes by the cumbersome title of [Problem Details for HTTP APIs](http://tools.ietf.org/html/draft-nottingham-http-problem-06)

##### API Problem

Nitro goes with the media type of `application/problem+json` and uses the API Problem specification to format it's exceptions, interestingly they have an XML variant as well but we only support the JSON. 

According to the API problem The payload of an API problem has following structure:

```json
HTTP/1.1 500 Internal Error
Content-Type: application/problem+json

{
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "detail": "Third party has forbidden the request to process the payment",
    "status": 500,
    "title": "Internal Server Error"
}
```

|Key|Description|Required|
|---|-----------|--------|
|`type`|URL to document describing the error condition|No|
|`title`|Brief title for the error condition|Yes|
|`staus`|HTTP Status code for the request| Yes|
|`detail`|Error detail specific to request|No|
|`instance`|URL identifying the specific instance of this error|No|


##### Exceptions included in the package

Nitro provides a set of exceptions out of the box which you can use and it will automatically handle the status code and title. And the JSON response will be generated as per the API problem specification. Also you can optionally (and you should) specify the detail of exception and change title.

```php
# $title is optional, if you do not provide it then default exception title will be used
throw new Nitro\Exceptions\BadRequestException($detail, $title);
throw new Nitro\Exceptions\ForbiddenException($detail, $title);
throw new Nitro\Exceptions\InternalErrorException($detail, $title);
throw new Nitro\Exceptions\NotFoundException($detail, $title);
throw new Nitro\Exceptions\NotModifiedException($detail, $title);
throw new Nitro\Exceptions\PreconditionFailedException($detail, $title);
throw new Nitro\Exceptions\ProcessingException($detail, $title);
throw new Nitro\Exceptions\RequestTooLongException($detail, $title);
throw new Nitro\Exceptions\NoContentException($detail, $title);
throw new Nitro\Exceptions\PaymentRequiredException($detail, $title);
throw new Nitro\Exceptions\RequestTimeoutException($detail, $title);

# Or a generic one
throw new Nitro\Exceptions\HttpException($title, $status, $detail);
```

##### Helpers for throwing exceptions

If you are extending your controllers from `Nitro\Controllers\BaseController` and your models from `Nitro\Models\BaseEloquent` or `Nitro\Models\BaseMoloquent` then you can easily throw exceptions from your controllers or models by the following (of course they will be turned into a  JSON response):

```php
$this->errorNotFound($detail, $title);
$this->errorBadRequest($detail, $title);
$this->errorForbidden($detail, $title);
$this->errorInternal($detail, $title);
$this->errorNoContent($detail, $title);
$this->errorInvalidParameters($detail, $title);
$this->errorEmptyParameters($detail, $title);
$this->errorTimeout($detail, $title);
$this->errorPaymentRequired($detail, $title);

# If you would like to throw a generic exception
$this->error($title, $statusCode, $detail);
```

##### Multiple ways of handling problems

In case you would like to throw an exception, you can do one of the following, throw exceptions using `throw` keyword directly e.g. 

```php
throw new Nitro\Exceptions\RequestTooLongException($detail, $title);
``` 

or you can use the `use` the `Nitro\Failable` trait in your class and then you can simply use the helpers specified above i.e.`

```php
use Nitro\Failable;

class Witch {

    use Failable;   // Use this trait
    
    public function createGold() {
    
        if (!$this->voodoo()) {
            $this->badRequest("No magic could have been detected");
            // $this->error("No magic detected", 400, "It was all an illusion, no gold could have been created");
        }
        ...
    }
}
```

#### e. Query Scopes

If you extend from the `BaseEloquent` or `BaseMoloquent` classes, there are certain commonly used query scopes provided in order to help you. Please find the details below on the scopes provided

##### i. byField($field, $value)

Easily add the equality conditions. For example, if you had a query like following:

```php
Witch::where('spells_count', '=', 10)->get();
```
Now you can write:
```php
Witch::byField('spells_count', 10)->get();
```

##### ii. filters($associativeArray)

This helps you in easily filtering by multiple equality conditions i.e. if you had a query like below:

```php
Witch::where('spells_count', '=', 10)->where('age', '=', 90)->where('location', '=', 'hut')->get()
```
Now you can write:
```php
Witch::filters([
    'spells_count' => 10,
    'age' => 90,
    'location' => 'hut'
])->get()
```

##### iii. `desc($field)`, `asc($field)`

If you had a query like

```php
Witch::where('spells_count', '=', 10)->where('age', '=', 90)->where('location', '=', 'hut')->orderBy('born_at', 'desc')->get()
```
Now you can write

```php
Witch::filters([
    'spells_count' => 10,
    'age' => 90,
    'location' => 'hut'
])->desc('born_at')->get()
```

#### f. Response

`BaseController` allows you to easily respond through your controllers. Inside your controller you can return through your action by calling any of the below methods to respond in a specific way. Please note that these methods can work with arrays, collections, model objects, length aware paginators, strings etc. 

*Note:* Please note that using these methods are not mandatory. If you return simple item/collection/model object/array etc from your controller action, it will automatically be converted to JSON and `200` status code will be returned.

The methods provided are:

##### i. `respond($content, $status)`

The generic response method, where `$content` is the content that you want to respond with i.e. which will be returned as json and `$status` is the status code. `200` will be used if none provided.

##### ii. `respondWithHeaders($content, $headers, $status)`

`200` status code will be returned if none provided.

##### iii. `transformAndRespond($content, $callback)`

If you have some data that you want to forcefuly transform before returning as `json` then use `$callback` i.e. provide the transform function and it will be used to transform the passed data before returning the content as JSON.

*Note:* Please note that this method is only to forcefuly transform the data with custom callback and you don't need to use this in case you would like to use the model transformer.

##### iv. `paginateAndRespond($collection, $perPage = 10)`

If you have a collection i.e. simple collection `Illuminate\Support\Collection` or eloquent collection `Illuminate\Database\Eloquent\Collection` or moloquent collection `Jenssegers\Mongodb\Collection` you can manually paginate and return the data. Also `page` parameter in the query string will be used to determine the content to return. That is the behavior will be the same as when you respond with the `LengthAwarePaginator` as response

##### v. `success(string|array $message)` with status code `200`

The data that you will pass to this method, will be put into the `data` key of the response and following format response will be returned

```php
[
    'status': 'success',
    'data': [
        // Application specific data that you will pass to success
    ]
]
```

##### vi. `created($response)` with status code `201`

If you provide `$response` as a string then the following format will be returned

```php
[
    'status': 'created',
    'data': 'Your response'
]
```

If you provide it with a model object it will simply be returned as JSON.

##### vii. `notModified($response)` with status code `304`

If you provide `$response` as a string then the following format will be returned

```php
[
    'status': 'not_modified',
    'data': 'Your response'
]
```
If you provide it with a model object it will simply be returned as JSON along with the 304 status code.

#### g. Model based Validation

The package allows you to easily validate the content based upon the model. All you have to do is specify the `$rules` property in the model that you want to validate. Please note that the `rules` is a multidimensional array where root keys specify the `scenario` and value is the array of rules. The default scenario is `default` and you should add the `default` key:

```php
class Booking extends BaseMoloquent 
{
    // Specify the validation rules in model
    protected $rules = [
        'default' => [
            'hotel_name' => 'required',
            'booking_ref' => 'alpha_num'
            'booking_date' => 'date|required'
        ],
        'mobile_booking' => [
            'hotel_name' => 'required',
            'booking_ref' => 'alpha_num|required',
            'booking_date' => 'date'
        ]
    ];
        
    // Property to set messages for if the validation fails (Optional)
    protected $messages = [
        'hotel_name.required' => 'Hotel name must be provided for booking',
        'booking_ref.alpha_num' => 'Invalid booking number',
        'booking_date.date' => 'Invalid booking date'
    ];
    
    // If you do not specify this property, the `default` validation will be used.
    protected $defaultValidation = 'default';
}

$booking = new Booking();
$params = $request->all(); // The input that you want to validate (must be array)

// Then you can simply do
// Note: In this case `mobile_booking` rules will be used. If it wasn't present then `default` rules
//       would have been used
if($booking->validate($params, 'mobile_booking')) {
    // Validation passed
} else {
    // Validation failed. You can get the errors by following:
    $errors = $booking->errors();
}
```

Have a look at the following link https://laravel.com/docs/5.1/validation#available-validation-rules for all the available validation rules. Also you can [create your custom validation rules](https://laravel.com/docs/5.1/validation#custom-validation-rules) and add them in the rules array

