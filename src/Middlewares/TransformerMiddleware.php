<?php

namespace Nitro\Middlewares;

use Closure;
use Illuminate\Http\Response;
use Nitro\Helpers\Transformer;

/**
 * Class TransformerMiddleware
 *
 * Middleware to automatically transform the response, in case the returned
 * data is model based.
 *
 * @package Nitro\Middlewares
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class TransformerMiddleware
{
    /**
     * TransformerMiddleware constructor.
     *
     * @param \Nitro\Helpers\Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        // Having the `original` property means that the model that the model
        // is being returned in the response. Transform based on model and reset
        // the content
        if (property_exists($response, 'original')) {
            $transformedContent = $this->transformer->transformModel($response->original);
            $response->setContent($transformedContent);
        }

        return $response;
    }
}
