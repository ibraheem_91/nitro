<?php

namespace Nitro\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Query\Builder;
use Nitro\Failable;

/**
 * Class BaseEloquent
 *
 * Any models that are going to be extending the `Eloquent` class should
 * extend from this class in order to reap the benefits of query scopes
 * etc
 *
 * @package Nitro\Models
 * @todo Need to add methods from query builder
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class BaseEloquent extends Eloquent
{
    // Any scopes that will be shared between eloquent and moloquent
    use BaseModelTrait;
    use Failable;

    /**
     * Assists in conditional query of getting the data for specified date of a field only
     * Usage Example:
     * For example to get all the users added on January 1st 2015, you will do
     * $query = User::byDay('created_at', '2015-01-01')->get();
     *                       -------------^-----------
     *
     * @param $query Builder
     * @param $field string
     * @param $value string
     *
     * @return mixed
     */
    function scopeByDay($query, $field, $value)
    {
        return $query->whereRaw("DATE(" . $field . ") = CAST(" . date("'Y-m-d'", strtotime($value)) . " as DATE)");
    }

    /**
     * Assists in applying the date range on a specified field
     *
     * Example Usage:
     * For example to get all the users added between, January 2015 and February 2015
     * $query = User::byDayRange('created_at', '2015-01-01', '2015-02-01')->get();
     *                ------------------------^-------------------------
     *
     * @param $query     Builder
     * @param $field     string
     * @param $startDate string
     * @param $endDate   string
     *
     * @return mixed
     */
    function scopeByDayRange($query, $field, $startDate, $endDate)
    {
        return $query->whereRaw("( DATE(" . $field . ") >= CAST(" . date("'Y-m-d'", strtotime($startDate)) . " as DATE) AND DATE(" . $field . ") <= CAST(" . date("'Y-m-d'", strtotime($endDate)) . " as DATE) )");
    }
}
