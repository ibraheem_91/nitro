<?php

namespace Nitro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;
use Jenssegers\Mongodb\Query\Builder;
use Nitro\Failable;

/**
 * Class BaseMoloquent
 *
 * All the moloquent models should extend from this model
 *
 * @package Nitro\Models
 *
 * @method static Builder distinct($column = false)
 * @method static boolean exists()
 * @method static mixed find($id, $columns = [])
 * @method static \Illuminate\Database\Query\Builder|static forPage($page, $perPage = 15)
 * @method static Builder from($collection)
 * @method static string generateCacheKey()
 * @method static array|static[] get($columns = [])
 * @method static array|static[] getFresh($columns = [])
 * @method static Builder hint($index)
 * @method static boolean insert(array $values)
 * @method static int insertGetId(array $values, $sequence = null)
 * @method static array lists($column, $key = null)
 * @method static Builder orderBy($column, $direction = 'asc')
 * @method static array pluck($column, $key = null)
 * @method static Builder project($columns)
 * @method static mixed raw($expression = null)
 * @method static Builder timeout($seconds)
 * @method static boolean truncate()
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder whereBetween($column, array $values, $boolean = 'and', $not = false)
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class BaseMoloquent extends Moloquent
{
    use BaseModelTrait;
    use Failable;

    /**
     * Gets the distinct values for the provided column
     *
     * @param string $columnName
     *
     * @return array
     */
    public function getDistinct($columnName)
    {
        $models = $this->distinct($columnName)->get([$columnName]);

        $tempValues = [];

        // Because the above query returns it as an array of arrays
        foreach ($models as $model) {
            $tempValues[] = __::get($model, 0);
        }

        // Remove any empty values
        return array_filter($tempValues);
    }
}
