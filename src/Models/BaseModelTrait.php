<?php

namespace Nitro\Models;

use __;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Nitro\Exceptions\BadRequestException;
use Nitro\Helpers\Validator as NitroValidator;

/**
 * Class BaseModelTrait
 *
 * Holds any functionality that is shared between moloquent and eloquent
 *
 * @package Nitro\Models
 * @method static Builder byField($fieldName, $fieldValue) @see scopeByField() in this class
 * @method static Builder filters($filtersArray) @see scopeFilters() in this class
 * @method static Builder desc($field) @see scopeDesc() in this class
 * @method static Builder asc($field) @see scopeAsc() in this class
 * @method static Builder fieldLike($field, $value) @see scopeFieldLike() in this class
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
trait BaseModelTrait
{
    // Validation rules, messages and the default scenario to be used
    protected $rules             = [];
    protected $messages          = [];
    protected $defaultValidation = 'default';

    /** @var  \Illuminate\Validation\Validator */
    protected $validator;

    /** @var MessageBag */
    protected $errors;

    /**
     * Adding an equality check on specific field
     * e.g.
     *     User::byField('name', 'Nitro')->get()
     *
     * @param $query Builder
     * @param $field string
     * @param $value string
     *
     * @return mixed
     */
    function scopeByField($query, $field, $value)
    {
        return $query->where($field, '=', $value);
    }

    /**
     * Assists in applying an array of equality filters
     * e.g.
     *      User::filters([
     *          'name' => 'Nitro',
     *          'responsibility' => 'Running'
     *      ]);
     *
     * This will find all the users with name nitro and responsibility running
     *
     * @param static $query
     * @param        $filters
     *
     * @return mixed
     */
    function scopeFilters($query, $filters)
    {
        foreach ($filters as $fieldName => $value) {
            /**
             * Query scope that laravel magically would generate
             *
             * @link https://laravel.com/docs/5.2/eloquent#query-scopes
             * @see  BaseModelTrait::scopeByField
             */
            $query->byField($fieldName, $value);
        }

        return $query;
    }

    /**
     * Sorts the query results in descending order for the specified field
     * e.g.
     *      User::filters([
     *          'role' => 'administrator'
     *      ])->desc('created_at')->get();
     *
     * @param $query Builder
     * @param $field
     *
     * @return mixed
     */
    function scopeDesc($query, $field)
    {
        return $query->orderBy($field, 'desc');
    }

    /**
     * Sorts the query results in ascending order for the specified field
     * e.g.
     *      User::filters([
     *          'role' => 'administrator'
     *      ])->asc('created_at')->get();
     *
     * @param $query Builder
     * @param $field string
     *
     * @return mixed
     */
    function scopeAsc($query, $field)
    {
        return $query->orderBy($field, 'asc');
    }

    /**
     * Applies the like filter on the query
     * e.g.
     *      User::fieldLike('role', 'administrator')->get();
     *
     * @param $query Builder
     * @param $field string
     * @param $value string
     *
     * @return mixed
     */
    function scopeByFieldLike($query, $field, $value)
    {
        return $query->where($field, 'like', "%" . $value . "%");
    }

    /**
     * Tries to apply the between filter using the given dates array
     *
     * @param $query
     * @param $field
     * @param $dates
     *
     * @return mixed
     */
    function scopeMightBetween($query, $field, $dates)
    {
        if (!empty($dates['from']) && !empty($dates['to'])) {
            $query = $query->where($field, '>=', $dates['from'])
                           ->where($field, '<=', $dates['to']);
        } else if (!empty($dates['from'])) {
            $query = $query->where($field, '>=', $dates['from']);
        } else if (!empty($dates['to'])) {
            $query = $query->where($field, '<=', $dates['to']);
        }

        return $query;
    }

    /**
     * Validates the input provided with the rules available in the model
     *
     * @param $data
     * @param $scenario
     *
     * @return bool
     */
    public function validate($data, $scenario = '')
    {
        $rules = $this->rules;

        // User did not provide any scenario, use the default scenario
        $scenario = empty($scenario) ? $this->defaultValidation : $scenario;

        // If the required scenario hasn't been set, use the rules as it may have
        // been specified as a normal array (i.e. in case there were no scenarios
        // and just a single scenario in that case there will be just rules array)
        $rules = !empty($rules[$scenario]) ? $rules[$scenario] : $rules;

        $this->validator = NitroValidator::make($data, $rules, $this->messages);

        // Check for failure
        if ($this->validator->fails()) {
            $this->errors = $this->validator->errors();

            return false;
        }

        // validation pass
        return true;
    }

    /**
     * Validation errors
     *
     * @param bool $throwException
     *
     * @return mixed
     * @throws \Nitro\Exceptions\BadRequestException
     */
    public function errors($throwException = false)
    {
        if (!empty($this->errors) && $throwException === true) {
            throw new BadRequestException($this->errors);
        }

        return $this->errors;
    }

    /**
     * Alias of setRawAttributes
     * set model attributes
     *
     * @param array $attributes
     *
     * @author Adnan Ahmed <adnan.ahmed@tajawal.com>
     *
     */
    public function setAttributes($attributes)
    {
        $this->setRawAttributes($attributes, false);
    }
}
