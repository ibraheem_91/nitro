<?php

namespace Nitro\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Laravel\Lumen\Routing\Controller;
use Nitro\Failable;
use Nitro\Helpers\Transformer;

/**
 * Class BaseController
 *
 * @package Nitro
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class BaseController extends Controller
{
    // So that we may be able to throw exceptions through any
    // controllers extending the base controller
    use Failable;

    /**
     * Returns the json response with headers
     *
     * @param       $content
     * @param array $headers
     * @param int   $status
     *
     * @return mixed
     */
    public function respondWithHeaders($content, $headers = [], $status = 200)
    {
        return response($content, $status)->withHeaders($headers);
    }

    /**
     * Transforms the provided content according to provided callback and returns it
     *
     * @param $content
     * @param $transformCallback
     *
     * @return array|void
     * @throws \Nitro\Exceptions\InternalErrorException
     */
    public function transformAndRespond($content, $transformCallback)
    {
        if (!is_callable($transformCallback)) {
            $this->errorInternal("Invalid callback provided in BaseController@transformRespond");
        }

        $transformer = new Transformer();

        return $transformer->forceTransform($content, $transformCallback);
    }

    /**
     * Automagically paginates the response
     *
     * @param \Illuminate\Support\Collection $content
     * @param int                            $perPage
     * @param int                            $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|void
     * @throws \Nitro\Exceptions\InternalErrorException
     */
    public function paginateAndRespond(Collection $content, $perPage = 10, $page = null)
    {
        if (!$content instanceof Collection) {
            $this->errorInternal('Data to paginate must be Collection');
        }

        $page = ($page == null) ? Input::get('page', 1) : $page;

        $paginator = new LengthAwarePaginator(
            $content->forPage($page, $perPage), $content->count(), $perPage, $page
        );

        return $paginator;
    }

    /**
     * Responds with the success response.
     *
     * @param $message
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function success($message)
    {
        $response = [
            'status' => 'success',
            'data'   => $message,
        ];

        return $this->respond($response);
    }

    /**
     * Returns the response.
     *
     * @param     $content
     * @param int $status
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respond($content, $status = 200)
    {
        return response($content, $status);
    }

    /**
     * Responds with the 201 status code and passed content
     *
     * @param $response
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function created($response)
    {
        // If it is a string or array
        if (is_string($response)) {
            $response = [
                'status' => 'created',
                'data'   => $response,
            ];
        }

        return $this->respond($response, 201);
    }

    /**
     * Responds with the 204 status code and passed content
     *
     * @param $response
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updated($response)
    {
        // If it is a string or array
        if (is_string($response)) {
            $response = [
                'status' => 'updated',
                'data'   => $response,
            ];
        }

        return $this->respond($response, 204);
    }

    /**
     * Responds with the 202 status code and passed content
     *
     * @param $response
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleted($response)
    {
        // If it is a string or array
        if (is_string($response)) {
            $response = [
                'status' => 'deleted',
                'data'   => $response,
            ];
        }

        return $this->respond($response, 202);
    }

    /**
     * Responds with the 304 status code and passed content
     *
     * @param $response
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function notModified($response)
    {
        // If it is a string or array
        if (is_string($response)) {
            $response = [
                'status' => 'not_modified',
                'data'   => $response,
            ];
        }

        return $this->respond($response, 304);
    }

    /**
     * Responds with the 422 status code and passed content
     *
     * @param $response
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function unProcessable($response)
    {
        // If it is a string or array
        if (is_string($response)) {
            $response = [
                'status' => 'un_processable',
                'data'   => $response,
            ];
        }

        return $this->respond($response, 422);
    }
}
