<?php

namespace Nitro\Exceptions;

/**
 * Class NotFoundException
 *
 *
 * @package App\Exceptions
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class InternalErrorException extends BaseException
{
    /**
     * @var string
     */
    protected $status = '500';
    protected $title  = 'Internal Error';
    protected $detail = '';

    /**
     * NotFoundException constructor.
     *
     * @param        $detail
     * @param string $title
     */
    public function __construct($detail, $title = '')
    {
        $this->detail = $detail ?: $this->detail;
        $this->title  = $title ?: $this->title;

        parent::__construct($this->detail);
    }
}
