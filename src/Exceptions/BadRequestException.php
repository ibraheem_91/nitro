<?php

namespace Nitro\Exceptions;

/**
 * Class BadRequestException
 *
 * @package App\Exceptions
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class BadRequestException extends BaseException
{
    /**
     * @var string
     */
    protected $status = '400';
    protected $title  = 'Bad Request';
    protected $detail = '';

    /**
     * BadRequestException constructor.
     *
     * @param string $detail
     * @param string $title
     */
    public function __construct($detail, $title = '')
    {
        $this->detail = $detail ?: $this->detail;
        $this->title  = $title ?: $this->title;

        parent::__construct($this->detail);
    }
}
