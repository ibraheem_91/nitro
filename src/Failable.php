<?php

namespace Nitro;

use Nitro\Exceptions\BadRequestException;
use Nitro\Exceptions\ForbiddenException;
use Nitro\Exceptions\HttpException;
use Nitro\Exceptions\InternalErrorException;
use Nitro\Exceptions\NoContentException;
use Nitro\Exceptions\NotFoundException;
use Nitro\Exceptions\PaymentRequiredException;
use Nitro\Exceptions\RequestTimeoutException;
use Nitro\Exceptions\UnauthorizedException;
use Nitro\Exceptions\UnprocessableEntityException;


/**
 * Class Failable
 *
 * Trait to be used on any class through which you want to throw exceptions. All you have to
 * do is use this trait and call the exception functions on that class.
 *
 * @package Nitro
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
trait Failable
{
    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param $title
     * @param $statusCode
     * @param $detail
     *
     * @throws \Nitro\Exceptions\HttpException
     */
    public function error($title = '', $statusCode = 500, $detail = null)
    {
        throw new HttpException($title, $statusCode, $detail);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param        $title
     *
     * @throws \Nitro\Exceptions\NotFoundException
     */
    public function errorNotFound($detail = '', $title = '')
    {
        throw new NotFoundException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param        $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\BadRequestException
     */
    public function errorBadRequest($detail = '', $title = '')
    {
        throw new BadRequestException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param        $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\ForbiddenException
     */
    public function errorForbidden($detail = '', $title = '')
    {
        throw new ForbiddenException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param        $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\UnauthorizedException
     */
    public function errorUnauthorized($detail = '', $title = '')
    {
        throw new UnauthorizedException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param        $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\InternalErrorException
     */
    public function errorInternal($detail = '', $title = '')
    {
        throw new InternalErrorException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\NoContentException
     */
    public function errorNoContent($detail = '', $title = '')
    {
        throw new NoContentException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\BadRequestException
     */
    public function errorInvalidParameters($detail = '', $title = '')
    {
        throw new BadRequestException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\UnprocessableEntityException
     */
    public function errorUnprocessableEntity($detail = '', $title = '')
    {
        throw new UnprocessableEntityException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\BadRequestException
     */
    public function errorEmptyParameters($detail = '', $title = '')
    {
        throw new BadRequestException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\RequestTimeoutException
     */
    public function errorTimeout($detail = '', $title = '')
    {
        throw new RequestTimeoutException($detail, $title);
    }

    /**
     * The exception is automatically catched by the handler and JSON is returned
     *
     * @param string $detail
     * @param string $title
     *
     * @throws \Nitro\Exceptions\PaymentRequiredException
     */
    public function errorPaymentRequired($detail = '', $title = '')
    {
        throw new PaymentRequiredException($detail, $title);
    }
}
