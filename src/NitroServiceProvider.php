<?php

namespace Nitro;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

/**
 * Class NitroServiceProvider
 *
 * @package Nitro
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class NitroServiceProvider extends ServiceProvider
{
    /** @var Application */
    protected $app;

    /**
     * Bootstrap the application services
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Nitro\Controllers\BaseController');
        $this->app->make('Nitro\Models\BaseEloquent');
        $this->app->make('Nitro\Models\BaseMoloquent');

        $this->setupThirdParties();
    }

    /**
     * Sets up any third party libraries etc
     * For the time being, it enables:
     *  - Moloquent
     *  - Eloquent
     */
    private function setupThirdParties()
    {
        // Register Moloquent
        $this->app->register('Jenssegers\Mongodb\MongodbServiceProvider');

        // Enable eloquent if previously disabled
        $this->app->withEloquent();
    }
}
