<?php
namespace Nitro\Helpers;

use Closure;
use Illuminate\Support\Facades\Validator as LumenValidator;
use Illuminate\Validation\PresenceVerifierInterface;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Validator
 *
 * Just a wrapper around Lumen's validator Facade to assist in code completions for IDe
 *
 * @method static void extend($rule, $extension, $message = null)
 * @method static void extendImplicit($rule, $extension, $message = null)
 * @method static void getPresenceVerifier()
 * @method static TranslatorInterface getTranslator()
 * @method static \Illuminate\Validation\Validator make($data, $rules, $messages = [], $customAttributes = [])
 * @method static void replacer($rule, $replacer)
 * @method static void resolver(Closure $resolver)
 * @method static PresenceVerifierInterface setPresenceVerifier(PresenceVerifierInterface $presenceVerifier)
 *
 * @see \Illuminate\Validation\Factory
 */
class Validator extends LumenValidator
{
}
