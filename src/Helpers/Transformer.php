<?php

namespace Nitro\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class Transformer
 *
 * @package Nitro\Helpers
 *
 * @author  Kamran Ahmed <kamran.ahmed@tajawal.com>
 */
class Transformer
{
    /**
     * Transforms the data passed based upon the `transform` function in the model
     *
     * @param array|Collection|LengthAwarePaginator $models
     *
     * @return array
     */
    public function transformModel($models)
    {
        // In case of an array or collection
        if (is_array($models) || $models instanceof Collection) {
            $transformedData = $this->transformObjects($models);
        } else if (is_object($models) && $this->canTransformObject($models)) { // In case of single object
            $transformedData = $models->transform($models);
        } else if ($models instanceof LengthAwarePaginator) {

            $transformedPaginator = $this->transformObjects($models->items());

            $transformedData = [
                'total'          => $models->total(),
                'per_page'       => $models->perPage(),
                'current_page'   => $models->currentPage(),
                'last_page'      => $models->lastPage(),
                'next_page_url'  => $models->nextPageUrl(),
                'prev_page_url'  => $models->previousPageUrl(),
                'has_pages'      => $models->hasPages(),
                'has_more_pages' => $models->hasMorePages(),
                'data'           => $transformedPaginator,
            ];
        } else {
            $transformedData = $models;
        }

        return $transformedData;
    }

    /**
     * Transforms an array or collection of objects
     *
     * @param array|Collection|LengthAwarePaginator $objects
     *
     * @return array
     */
    public function transformObjects($objects)
    {
        $transformed = [];

        foreach ($objects as $key => $object) {
            /**
             * For each of the objects, check if the object has transform() method in it, if it exists
             * pass it the item and get it transformed, otherwise use the same object
             *
             * @see https://github.com/tajawal/nitro#a-automatic-transformers
             */
            $transformed[$key] = $this->canTransformObject($object) ? $object->transform($object) : $object;
        }

        return $transformed;
    }

    /**
     * Checks whether we can transform the object or not
     *
     * @param $object
     *
     * @return bool
     */
    public function canTransformObject($object)
    {
        return is_object($object) && method_exists($object, 'transform');
    }

    /**
     * Transforms the data based upon the custom provided callback
     *
     * @param $content
     * @param $callback
     *
     * @return array
     */
    public function forceTransform($content, $callback)
    {
        $transformedData = [];

        // If it is an iterateable content
        if (is_array($content) || $content instanceof Collection) {
            foreach ($content as $key => $item) {
                $transformedData[$key] = $callback($item);
            }
        } else if (is_object($content)) { // In case of single object
            $transformedData = $callback($content);
        } else if ($content instanceof LengthAwarePaginator) { // In case it is paginated data

            $transformed = [];

            foreach ($content->items() as $key => $item) {
                $transformed[$key] = $callback($item);
            }

            $transformedData = [
                'total'          => $content->total(),
                'per_page'       => $content->perPage(),
                'current_page'   => $content->currentPage(),
                'last_page'      => $content->lastPage(),
                'next_page_url'  => $content->nextPageUrl(),
                'prev_page_url'  => $content->previousPageUrl(),
                'has_pages'      => $content->hasPages(),
                'has_more_pages' => $content->hasMorePages(),
                'data'           => $transformed,
            ];
        } else {
            $transformedData = $content;
        }

        return $transformedData;
    }
}
