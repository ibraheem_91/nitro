<?php
namespace Nitro\Helpers;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

/**
 * Class DateHelper
 *
 * @package Nitro\Helpers
 */
class DateHelper
{
    /**
     * Converts the datetime to mongodb date object. Expects that the
     * given datetime is a string and is any format that strtotime understands
     *
     * @param string $dateTime
     *
     * @return \Nitro\Helpers\UTCDatetime
     */
    public static function getMongoDate($dateTime = 'now')
    {
        $seconds = strtotime($dateTime);
        if ($seconds === false) {
            return false;
        }

        // Multiplying by 1000 because it expects milliseconds
        return new UTCDatetime($seconds * 1000);
    }

    /**
     * Converts the epoch time to mongoid. Useful for generating
     * temporary mongo IDs as well as to implement date comparisons
     * on _id field.
     *
     * @param $timeString
     *
     * @return \MongoDB\BSON\ObjectID
     */
    public static function epochToMongoId($timeString)
    {
        // Turn it into hex
        $hexTime = dechex($timeString);

        // Pad it out to 8 chars
        $hexTime = str_pad($hexTime, 8, '0', STR_PAD_LEFT);

        // Make an _id from it for Mongodb
        return new ObjectID($hexTime . '0000000000000000');
    }
}
